# Doghouse Background Image
# `⌒°(❛ᴗ❛)°⌒`

##### What it does
  - Makes an image into a background image.

##### How to install
 - Ensure you have the doghouse packages repository in your `composer.json`
```
"repositories": [
        ...
        {
            "type": "composer",
            "url": "http://packages.doghouse.agency/"
        }
```
  - In terminal, run: `$ composer require doghouse/doghouse_background_image`
  - Enable the module (EG `drush en doghouse_background_image`)
  -- The module will be located in `modules/contrib` 

##### How to use it
  - Add a wrapper to the image with 'js-img-to-bg' class. 
# `~(=^‥^)ノ☆`
