(function ($, Drupal) {

  'use strict';

  /**
   * Turn an image into a background-image on its parent. Img el remains but
   * set to visibility: hidden to act as a hidden spacer and honor AR.
   */
  Drupal.behaviors.imgToBg = {
    attach: function(context, settings) {
      $('.js-img-to-bg', context)
        .once('img-to-bg')
        .each(function(i, d){
          $(this).imgToBg(this);
        });
    }
  };

  /**
   * jQuery plugin for ImgToBg.
   */
  $.fn.imgToBg = function() {
    var $img;
    $img = this.find('img').first();
    if ($img.length > 0) {
      $img.css('visibility', 'hidden');
      this.css('background-image', 'url("' + $img.attr('src') + '")');
    }
  }

})(jQuery, Drupal);